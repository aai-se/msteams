package com.automationanywhere.botcommand.msteams;

import java.net.MalformedURLException;
import java.util.Arrays;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.microsoft.aad.msal4j.ClientCredentialFactory;
import com.microsoft.aad.msal4j.ClientCredentialParameters;
import com.microsoft.aad.msal4j.ConfidentialClientApplication;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.aad.msal4j.PublicClientApplication;
import com.microsoft.aad.msal4j.UserNamePasswordParameters;
import com.microsoft.graph.auth.confidentialClient.ClientCredentialProvider;
import com.microsoft.graph.auth.enums.NationalCloud;
import com.microsoft.graph.auth.publicClient.UsernamePasswordProvider;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.GraphServiceClient;

public class MSAuthHelper {
	
		private static String applicationId;
		
		private static final NationalCloud NATIONAL_CLOUD = NationalCloud.Global;
		
		private static final List<String> SCOPES = Arrays.asList( "https://graph.microsoft.com/.default","offline_access");
	
	
	    // Set authority to allow only organizational accounts
	    // Device code flow only supports organizational accounts
	    private static String authority ;

	    public static void initialize(String authority,String applicationId) {
	        MSAuthHelper.applicationId = applicationId;
	        MSAuthHelper.authority = authority;
	    }

	    public static String getAccessTokenbyUserPassword( String user, String password, String[] scopes) {
	        if (applicationId == null) {
	            System.out.println("You must initialize Authentication before calling getUserAccessToken");
	            return null;
	        }

	        Set<String> scopeSet = Set.of(scopes);

	        ExecutorService pool = Executors.newFixedThreadPool(1);
	        PublicClientApplication app;
	        try {
	            // Build the MSAL application object with
	            // app ID and authority
	            app = PublicClientApplication.builder(applicationId)
	                .authority(authority)
	                .executorService(pool)
	                .build();
	        } catch (MalformedURLException e) {
	            return null;
	        }
	        // Request a token, passing the requested permission scopes
	        IAuthenticationResult result = app.acquireToken(
	        		UserNamePasswordParameters.builder(scopeSet, user, password.toCharArray()).build()
	        ).exceptionally(ex -> {
	            System.out.println("Unable to authenticate - " + ex.getMessage());
	            return null;
	        }).join();

	        pool.shutdown();

	        if (result != null) {
	            return result.accessToken();
	        }

	        return null;
	    }
	    
	    
	    
	    public static String getAccessTokenbyClientSecret( String clientId, String secret, String[] scopes) {
	        if (applicationId == null) {
	            System.out.println("You must initialize Authentication before calling getUserAccessToken");
	            return null;
	        }

	        Set<String> scopeSet = Set.of(scopes);

	        ExecutorService pool = Executors.newFixedThreadPool(1);
	        ConfidentialClientApplication app;
	        try {
	        	 app = ConfidentialClientApplication.builder(
	                     clientId,
	                     ClientCredentialFactory.createFromSecret(secret))
	                     .authority(authority)
	                     .build();
	        } catch (MalformedURLException e) {
	            return null;
	        }

	        // Request a token, passing the requested permission scopes
	        IAuthenticationResult result = app.acquireToken(
	        		ClientCredentialParameters.builder(scopeSet).build()
	        ).exceptionally(ex -> {
	            System.out.println("Unable to authenticate - " + ex.getMessage());
	            return null;
	        }).join();

	        pool.shutdown();

	        if (result != null) {
	            return result.accessToken();
	        }

	        return null;
	    }
	    
	    
	    public static IGraphServiceClient newClientbyClientCredential(String clientid, String tenantid, String clientsecret) {
	    	ClientCredentialProvider authProvider = new ClientCredentialProvider(clientid, SCOPES, clientsecret, tenantid, null);
			IGraphServiceClient graphClient = GraphServiceClient
					.builder()
					.authenticationProvider(authProvider)
					.buildClient();
			 graphClient.users().buildRequest().get();
	      return graphClient;
		
	    }
	    
	    public static IGraphServiceClient newClientbyUserCredentials(String clientid, String user ,String password, String tenantid,String clientsecret) {
			UsernamePasswordProvider authProvider = new UsernamePasswordProvider(clientid, SCOPES, user, password, null, tenantid, clientsecret);
			IGraphServiceClient graphClient = GraphServiceClient
					.builder()
					.authenticationProvider(authProvider)
					.buildClient();
			 graphClient.users().buildRequest().get();
	      return graphClient;
		
	    }
}
