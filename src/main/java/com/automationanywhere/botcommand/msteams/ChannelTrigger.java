/*
 * Copyright (c) 2020 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

/**
 * 
 */
package com.automationanywhere.botcommand.msteams;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.StartListen;
import com.automationanywhere.commandsdk.annotations.StopAllTriggers;
import com.automationanywhere.commandsdk.annotations.StopListen;
import com.automationanywhere.commandsdk.annotations.TriggerId;
import com.automationanywhere.commandsdk.annotations.TriggerRunnable;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThan;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.NumberInteger;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.microsoft.graph.models.extensions.Channel;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;

/**
 * There will be a singleton instance of this class. Whenever a new trigger is
 * created it comes with a triggerId. In this sample we create multiple tasks
 * and store them in a map with these triggerId as the key. Once the condition
 * of trigger matches we call the run method of the runnable to signal the
 * trigger.
 * <p>
 * In the following example we will create a trigger which triggers are user
 * specified intervals.
 * 
 * @author Stefan Karsten
 *
 */
@BotCommand(commandType = BotCommand.CommandType.Trigger)
@CommandPkg(label = "Channel Trigger", description = "Channel Trigger", icon = "email.svg", name = "teamschanneltrigger")
public class ChannelTrigger {

	// Map storing multiple tasks
	private static final Map<String, TimerTask> taskMap = new ConcurrentHashMap<>();
	private static final Timer TIMER = new Timer(true);
	private ZonedDateTime lastRun;
	
	private static Logger logger = LogManager.getLogger(ChannelTrigger.class);
	
	@TriggerId
	private String triggerUid;
	@TriggerRunnable
	private Runnable runnable;
	

	/*
	 * Starts the trigger.
	 */
	@StartListen
	public void startTrigger(
							@Idx(index = "1", type = AttributeType.CREDENTIAL) @Pkg(label = "User Name",  default_value_type = STRING) @NotEmpty SecureString username,
							@Idx(index = "2", type = AttributeType.CREDENTIAL) @Pkg(label = "Password",  default_value_type = STRING) @NotEmpty SecureString password,
							@Idx(index = "3", type = AttributeType.CREDENTIAL) @Pkg(label = "Tenant ID",  default_value_type = STRING) @NotEmpty SecureString tenantid,
							@Idx(index = "4", type = AttributeType.CREDENTIAL) @Pkg(label = "Client ID",  default_value_type = STRING) @NotEmpty SecureString clientid,
							@Idx(index = "5", type = AttributeType.CREDENTIAL) @Pkg(label = "Client Secret Key",  default_value_type = STRING) @NotEmpty SecureString secretkey,
							@Idx(index = "6", type = AttributeType.TEXT) @Pkg(label = "Team ID", default_value_type = DataType.STRING) @NotEmpty String teamID,
							@Idx(index = "7", type = AttributeType.TEXT) @Pkg(label = "Channel ID", default_value_type = DataType.STRING) @NotEmpty String channelID,
							@Idx(index = "8", type = AttributeType.NUMBER) @Pkg(label = "Please provide the interval in seconds", default_value = "120", default_value_type = DataType.NUMBER) @GreaterThan("0") @NumberInteger @NotEmpty Double interval) {
		
		
		    lastRun = ZonedDateTime.now();
		    logger.info("Trigger for "+username.getInsecureString()+" started");
			TimerTask timerTask = new TimerTask() {
			
			@Override
			public void run() {
				IGraphServiceClient client;
				Channel channel;
			    logger.info("Last run at"+lastRun.toString());
				try {
					client = MSAuthHelper.newClientbyUserCredentials(clientid.getInsecureString(), username.getInsecureString(), password.getInsecureString(),tenantid.getInsecureString(), secretkey.getInsecureString());
					channel = client.teams(teamID).channels(channelID)
					    .buildRequest()
					    .get();
					
				    logger.info("Channel "+channel.displayName);
				}
				catch (Exception e) {
					throw new BotCommandException("Authentication failed");
				}
				if (channel != null) {
					List<Option> options = new LinkedList<Option>();
					options.add(new QueryOption("$filter","lastModifiedDateTime gt "+lastRun.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)));
					TableValue tablevalue = MessageHelper.getDeltaMessages( client.teams(teamID).channels(channelID).messages().delta().buildRequest(options).get());
					lastRun = ZonedDateTime.now();
				    logger.info("Table rows "+tablevalue.get().getRows().size() );
					if (tablevalue.get().getRows().size() > 0) {
					  runnable.run();
					  return;
					}
				}
				else {
			        throw new BotCommandException("Channel does not exist");
				}
			}
					
		};
			

		taskMap.put(this.triggerUid, timerTask);
		TIMER.schedule(timerTask, interval.longValue()*1000, interval.longValue()*1000);
	}

	/*
	 * Cancel all the task and clear the map.
	 */
	@StopAllTriggers
	public void stopAllTriggers() {
		taskMap.forEach((k, v) -> {
			if (v.cancel()) {
				taskMap.remove(k);
			}
		});
	}

	/*
	 * Cancel the task and remove from map
	 *
	 * @param triggerUid
	 */
	@StopListen
	public void stopListen(String triggerUid) {
		if (taskMap.get(triggerUid).cancel()) {
			taskMap.remove(triggerUid);
		}
	}

	public String getTriggerUid() {
		return triggerUid;
	}

	public void setTriggerUid(String triggerUid) {
		this.triggerUid = triggerUid;
	}

	public Runnable getRunnable() {
		return runnable;
	}

	public void setRunnable(Runnable runnable) {
		this.runnable = runnable;
	}

}
