/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Channel;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.ChatMessage;
import com.microsoft.graph.models.extensions.ConversationMember;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.Team;
import com.microsoft.graph.requests.extensions.ChannelCollectionPage;
import com.microsoft.graph.requests.extensions.ConversationMemberCollectionPage;
import com.microsoft.graph.requests.extensions.IChannelCollectionPage;
import com.microsoft.graph.requests.extensions.IChannelCollectionRequestBuilder;
import com.microsoft.graph.requests.extensions.IChatMessageCollectionPage;
import com.microsoft.graph.requests.extensions.IConversationMemberCollectionPage;
import com.microsoft.graph.requests.extensions.IConversationMemberCollectionRequestBuilder;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.AttributeType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Joined Team Details", node_label = "Joined Team Details", 
label = "Joined Team Details", description = "Get Team Details, Members and Channels", 
name = "teamsteamdetails", icon = "pkg.svg", return_type = DataType.DICTIONARY, comment = true ,background_color = "#302c89"  , return_description = "Team Details Dictonary, key 'members' contains member ID list, key 'channels' contains channel ID list", return_required = true)
public class GetTeamsTeamDetails{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public DictionaryValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "MSTeamsSession") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Team ID", default_value_type = STRING) @NotEmpty String teamID) {

    	HashMap<String,Value> details = new HashMap<String,Value>();
		
   	 	IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		Team team = graphClient.teams(teamID)
			    .buildRequest()
			    .get();
		
		if (team != null)
		{
			details.put("id", new StringValue(team.id));
			details.put("description",  new StringValue((team.description != null) ? team.description: "" ));
			details.put("displayName",  new StringValue((team.displayName != null) ? team.displayName : "" ));
			details.put("isArchived",  new BooleanValue((team.isArchived != null) ? team.isArchived : false ));
			
			
			IConversationMemberCollectionPage members = graphClient.teams(teamID).members()
				    .buildRequest()
				    .get();
			
			List<Value> memberList = new ArrayList<Value>();
			for (Iterator iterator = members.getCurrentPage().iterator(); iterator.hasNext();) {
				ConversationMember conversationMember = (ConversationMember) iterator.next();
				memberList.add(new StringValue(conversationMember.id));				
			}
			
			while (members.getNextPage() != null) {
				members = members.getNextPage().buildRequest().get();
				for (Iterator iterator = members.getCurrentPage().iterator(); iterator.hasNext();) {
					ConversationMember conversationMember = (ConversationMember) iterator.next();
					memberList.add(new StringValue(conversationMember.id));
				}
			}
			ListValue memberListValue = new ListValue();
			memberListValue.set(memberList);
			details.put("members",memberListValue);
			
			
			
			IChannelCollectionPage channels = graphClient.teams(teamID).channels()
				    .buildRequest()
				    .get();
			List<Value> channelList = new ArrayList<Value>();
			for (Iterator iterator = channels.getCurrentPage().iterator(); iterator.hasNext();) {
				Channel channel = (Channel) iterator.next();
				channelList.add(new StringValue(channel.id));				
			}
			

			while (channels.getNextPage() != null) {
				channels = channels.getNextPage().buildRequest().get();
				for (Iterator iterator = channels.getCurrentPage().iterator(); iterator.hasNext();) {
					Channel channel = (Channel) iterator.next();
					channelList.add(new StringValue(channel.id));
				}
			}
			ListValue channelListValue = new ListValue();
			channelListValue.set(channelList);
			details.put("channels",channelListValue);
			
			
			
			
		}
		
	

		
		DictionaryValue returnValue = new DictionaryValue();
		returnValue.set(details);
		return returnValue;
			
	}
	
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
