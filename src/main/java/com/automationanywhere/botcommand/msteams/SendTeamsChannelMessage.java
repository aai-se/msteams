/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;


import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Channel;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.ChatMessage;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.ItemBody;
import com.microsoft.graph.models.generated.BodyType;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Send Channel Message", node_label = "Send Channel Message", 
label = "Send Teams Channel Message", description = "Send a Teams Channel Message", 
name = "teamschannelmessage", icon = "pkg.svg", return_type = STRING, comment = true ,background_color = "#302c89"  )
public class SendTeamsChannelMessage{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,default_value = "MSTeamsSession") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Team ID", default_value_type = STRING ) @NotEmpty String teamID,
            @Idx(index = "3", type = TEXT) @Pkg(label = "Channel ID", default_value_type = STRING ) @NotEmpty String channelID,
            @Idx(index = "4", type = AttributeType.TEXTAREA) @Pkg(label = "Chat Message", default_value_type = DataType.STRING) @NotEmpty String message,
            @Idx(index = "5", type = AttributeType.RADIO, options = {
					@Idx.Option(index = "5.1", pkg = @Pkg(label = "HTML", value = "HTML")),
					@Idx.Option(index = "5.2", pkg = @Pkg(label = "Text", value = "TEXT"))
		   			}) @Pkg(label = "Format", default_value = "TEXT", default_value_type = STRING) @NotEmpty String format,
            @Idx(index = "6", type = AttributeType.TEXT) @Pkg(label = "Reply To Message ID", default_value_type = DataType.STRING) String messageID
			) {


		messageID = (messageID != null) ? messageID : "";
		
		IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		Channel channel = graphClient.teams(teamID).channels(channelID)
			    .buildRequest()
			    .get();
		
		if (channel != null)
		{

			ChatMessage chatMessage = new ChatMessage();
			ItemBody body = new ItemBody();
			body.content = message;
			body.contentType = (format == "TEXT") ? BodyType.TEXT : BodyType.HTML;
			chatMessage.body = body;
			ChatMessage result;
			if (messageID != "") {
				result = graphClient.teams(teamID).channels(channelID).messages(messageID).replies()
					    .buildRequest()
					    .post(chatMessage);
			}
			else {
				result = graphClient.teams(teamID).channels(channelID).messages()
				    .buildRequest()
				    .post(chatMessage);
			}
			return new StringValue(result.body.content);
		}
		else {
			return new StringValue("");
		}

			
	}
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
