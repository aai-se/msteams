/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.ChatMessage;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.IChatMessageCollectionPage;


import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.AttributeType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.time.ZonedDateTime;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Chat Details", node_label = "Chat Details", 
label = "Chat Details", description = "Get Chat Details and Messages", 
name = "teamschatdetails", icon = "pkg.svg", return_type = DataType.DICTIONARY, comment = true ,background_color = "#302c89" , return_description = "Chat Details Dictonary, optional key 'messages' contains table of messages", return_required = true)
public class GetTeamsChatDetails{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public DictionaryValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "MSTeamsSession") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Chat ID", default_value_type = STRING) @NotEmpty String chatID,
            @Idx(index = "3", type = BOOLEAN) @Pkg(label = "Include Chat Messages", default_value_type = DataType.BOOLEAN, default_value = "false") @NotEmpty Boolean getMessages,
            @Idx(index = "4", type = AttributeType.VARIABLE) @Pkg(label = "Include only Messages after value of DateTime variable", default_value_type = DataType.DATETIME) ZonedDateTime last) {

    	HashMap<String,Value> details = new HashMap<String,Value>();
		
   	 	IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		Chat chat = graphClient.chats(chatID)
			    .buildRequest()
			    .get();
		
		if (chat != null)
		{

			details.put("id", new StringValue(chat.id));
			details.put("type",  new StringValue((chat.chatType.name() != null) ? chat.chatType.name() : "" ));
			details.put("topic",  new StringValue((chat.topic != null) ? chat.topic : "" ));
			details.put("created",  new DateTimeValue(chat.createdDateTime.toInstant().atZone(ZonedDateTime.now().getZone())));
			details.put("updated", new DateTimeValue(chat.lastUpdatedDateTime.toInstant().atZone(ZonedDateTime.now().getZone())));
		}
		
		if (getMessages) {
	
			
			TableValue tablevalue = MessageHelper.getMessages( graphClient.chats(chat.id).messages().buildRequest().get(),last);
			details.put("messages", tablevalue);
			

		}

		
		DictionaryValue returnValue = new DictionaryValue();
		returnValue.set(details);
		return returnValue;
			
	}
	
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
