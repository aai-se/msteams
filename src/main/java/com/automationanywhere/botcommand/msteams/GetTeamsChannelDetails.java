/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.RecordValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.record.Record;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Channel;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.ChatMessage;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.IChatMessageCollectionPage;
import com.microsoft.graph.requests.extensions.IChatMessageDeltaCollectionPage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.AttributeType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Channel Details", node_label = "Channel Details", 
label = "Channel Details", description = "Get Channel Details and Messages", 
name = "teamschanneldetails", icon = "pkg.svg", return_type = DataType.DICTIONARY, comment = true ,background_color = "#302c89"  , return_description = "Channel Details Dictonary, optional key 'messages' contains table of messages", return_required = true)
public class GetTeamsChannelDetails{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public DictionaryValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "MSTeamsSession") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Team ID", default_value_type = STRING) @NotEmpty String teamID,
            @Idx(index = "3", type = TEXT) @Pkg(label = "Channel ID", default_value_type = STRING) @NotEmpty String channelID,
            @Idx(index = "4", type = BOOLEAN) @Pkg(label = "Include Chat Messages", default_value_type = DataType.BOOLEAN, default_value = "false") @NotEmpty Boolean getMessages,
            @Idx(index = "5", type = AttributeType.VARIABLE) @Pkg(label = "Include only Messages after value of DateTime variable", default_value_type = DataType.DATETIME) ZonedDateTime last) {


    	HashMap<String,Value> details = new HashMap<String,Value>();
		
   	 	IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		Channel channel = graphClient.teams(teamID).channels(channelID)
			    .buildRequest()
			    .get();
		
		if (channel != null)
		{
			details.put("id", new StringValue(channel.id));
			details.put("description",  new StringValue((channel.description != null) ? channel.description : "" ));
			details.put("displayname",  new StringValue((channel.displayName != null) ? channel.displayName : "" ));
			details.put("email",  new StringValue((channel.email != null) ? channel.email : "" ));
			details.put("webURL",  new StringValue((channel.webUrl != null) ? channel.webUrl : "" ));
			details.put("created",  new DateTimeValue(channel.createdDateTime.toInstant().atZone(ZonedDateTime.now().getZone())));
		
			if (getMessages) {
				List<Option> options = new LinkedList<Option>();
				if (last != null) {
					options.add(new QueryOption("$filter","lastModifiedDateTime gt "+last.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)));
				}
				TableValue tablevalue = MessageHelper.getDeltaMessages( graphClient.teams(teamID).channels(channelID).messages().delta().buildRequest(options).get());
				details.put("messages", tablevalue);

			}
		} else {
			throw new BotCommandException("Channel does not exists");
		}

		
		DictionaryValue returnValue = new DictionaryValue();
		returnValue.set(details);
		return returnValue;
			
	}
	
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
