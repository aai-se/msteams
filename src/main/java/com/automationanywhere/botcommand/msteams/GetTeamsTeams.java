/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.Team;
import com.microsoft.graph.requests.extensions.IChatCollectionPage;
import com.microsoft.graph.requests.extensions.ITeamCollectionWithReferencesPage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Team IDs", node_label = "Get Joined Teams", 
label = "Get Joined Teams", description = "Get all Teams IDs in Teams", 
name = "teamsteams", icon = "pkg.svg", return_type = DataType.LIST, comment = true ,background_color = "#302c89"  ,return_required = true)
public class GetTeamsTeams{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public ListValue<String> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,default_value = "MSTeamsSession") @NotEmpty String sessionName) {

    	List<Value> teamIDs = new ArrayList<Value>();
		
   	 	IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		
		ITeamCollectionWithReferencesPage joinedTeams = graphClient.me().joinedTeams()
			    .buildRequest()
			    .get();
		
		for (Iterator iterator = joinedTeams.getCurrentPage().iterator(); iterator.hasNext();) {
			Team team = (Team) iterator.next();
			teamIDs.add(new StringValue(team.id));
		}
	
		while (joinedTeams.getNextPage() != null) {
			joinedTeams = joinedTeams.getNextPage().buildRequest().get();
			for (Iterator iterator = joinedTeams.getCurrentPage().iterator(); iterator.hasNext();) {
				Team team = (Team) iterator.next();
				teamIDs.add(new StringValue(team.id));

			}
		}

	
		
		ListValue returnValue = new ListValue();
		returnValue.set(teamIDs);
		return returnValue;
			
	}
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
