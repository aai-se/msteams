/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.Team;
import com.microsoft.graph.models.extensions.UsageDetails;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.requests.extensions.IChatCollectionPage;
import com.microsoft.graph.requests.extensions.ITeamCollectionWithReferencesPage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Signed User Details", node_label = "Get Signed User Details", 
label = "Get Signed User Details", description = "Get Teams Signed User Details", 
name = "teamsme", icon = "pkg.svg", return_type = DataType.DICTIONARY, comment = true ,background_color = "#302c89"  ,return_required = true)
public class GetTeamsMe{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public DictionaryValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,default_value = "MSTeamsSession") @NotEmpty String sessionName ) {


		HashMap<String,Value> details = new HashMap<String,Value>();
   	 	IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		
		User user = graphClient.me().buildRequest().get();
		
		if (user != null) {
			details.put("id",new StringValue(user.id));
			details.put("country",new StringValue((user.country != null) ? user.country : ""));
			details.put("city",new StringValue((user.city!= null) ? user.city : ""));
			details.put("companyName",new StringValue((user.companyName!= null) ? user.companyName : ""));
			details.put("department",new StringValue((user.department!= null) ? user.department: ""));
			details.put("givenName",new StringValue((user.displayName!= null) ? user.givenName: ""));
			details.put("jobTitle",new StringValue((user.jobTitle!= null) ? user.jobTitle: ""));
			details.put("mail",new StringValue((user.mail!= null) ? user.mail: ""));
			details.put("mobilePhone",new StringValue((user.mobilePhone!= null) ? user.mobilePhone: ""));
			details.put("offficeLocation",new StringValue((user.officeLocation!= null) ? user.officeLocation: ""));
			details.put("postalCode",new StringValue((user.postalCode!= null) ? user.postalCode: ""));
			details.put("state",new StringValue((user.state!= null) ? user.state: ""));
			details.put("streetAddress",new StringValue((user.streetAddress!= null) ? user.streetAddress: ""));
			details.put("surname",new StringValue((user.surname!= null) ? user.surname: ""));
			details.put("userPrincipalName",new StringValue((user.userPrincipalName!= null) ? user.userPrincipalName: ""));
			
		
		}

	
		
		DictionaryValue returnValue = new DictionaryValue();
		returnValue.set(details);
		return returnValue;
			
	}
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
