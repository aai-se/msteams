package com.automationanywhere.botcommand.msteams;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.RecordValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.record.Record;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.microsoft.graph.http.IBaseCollectionPage;
import com.microsoft.graph.models.extensions.ChatMessage;
import com.microsoft.graph.requests.extensions.IChatMessageCollectionPage;
import com.microsoft.graph.requests.extensions.IChatMessageCollectionRequestBuilder;
import com.microsoft.graph.requests.extensions.IChatMessageDeltaCollectionPage;


public class MessageHelper {

	public static TableValue getMessages(IChatMessageCollectionPage chatMessages,ZonedDateTime last) {
		
			List<Row> rows = new ArrayList<Row>();
		
			List<Schema> schemas = new ArrayList<Schema>();
			Schema schema =  new Schema();
			schema.setName("UserID"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("MessageID"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("Message"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("Created"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("Type"); 
			schemas.add(schema);
			schema =  new Schema();
			schema.setName("ReplyTo"); 
			schemas.add(schema);
	
	
			ZoneId zoneId = (last != null) ? last.getZone() : ZonedDateTime.now().getZone();
		
		  for (Iterator iterator = chatMessages.getCurrentPage().iterator(); iterator.hasNext();) {
			boolean add = true;
			ChatMessage chatMessage = (ChatMessage) iterator.next();
			ZonedDateTime createdDate = chatMessage.createdDateTime.toInstant().atZone(zoneId);
			if (last != null) {
                add = createdDate.isAfter(last);
			}
	
			if (add) {
				List<Value> values = new ArrayList<Value>();
				values.add(new StringValue(chatMessage.from.user.id));
				values.add(new StringValue(chatMessage.id));
				values.add(new StringValue(chatMessage.body.content));
				values.add(new DateTimeValue(createdDate));
				values.add(new StringValue(chatMessage.messageType.name()));
				values.add(new StringValue((chatMessage.replyToId) == null ? "" : chatMessage.replyToId));
				rows.add(new Row(values));
			}
		}	
	
		while (chatMessages.getNextPage() != null) {
			chatMessages = chatMessages.getNextPage().buildRequest().get();
			for (Iterator iterator = chatMessages.getCurrentPage().iterator(); iterator.hasNext();) {
				boolean add = true;
				ChatMessage chatMessage = (ChatMessage) iterator.next();
				ZonedDateTime createdDate = chatMessage.createdDateTime.toInstant().atZone(zoneId);
				if (last != null) {
	                add = createdDate.isAfter(last);
				}
		
				if (add) {
					List<Value> values = new ArrayList<Value>();
					values.add(new StringValue(chatMessage.from.user.id));
					values.add(new StringValue(chatMessage.id));
					values.add(new StringValue(chatMessage.body.content));
					values.add(new DateTimeValue(chatMessage.createdDateTime.toInstant().atZone(zoneId)));
					values.add(new StringValue(chatMessage.messageType.name()));
					values.add(new StringValue((chatMessage.replyToId) == null ? "" : chatMessage.replyToId));
					rows.add(new Row(values));
				}
			}
		}
	
		Table table = new Table();
		table.setRows(rows);
		table.setSchema(schemas);

		TableValue tableValue= new TableValue();
		tableValue.set(table);
	
		return tableValue;
	}
	

	
	public static TableValue getDeltaMessages(IChatMessageDeltaCollectionPage chatMessages) {
		
		List<Row> rows = new ArrayList<Row>();
	
		List<Schema> schemas = new ArrayList<Schema>();
		Schema schema =  new Schema();
		schema.setName("UserID"); 
		schemas.add(schema);
		schema =  new Schema();
		schema.setName("MessageID"); 
		schemas.add(schema);
		schema =  new Schema();
		schema.setName("Message"); 
		schemas.add(schema);
		schema =  new Schema();
		schema.setName("Created"); 
		schemas.add(schema);
		schema =  new Schema();
		schema.setName("Type"); 
		schemas.add(schema);
		schema =  new Schema();
		schema.setName("ReplyTo"); 
		schemas.add(schema);

		ZoneId zoneId =  ZonedDateTime.now().getZone();
		
		for (Iterator iterator = chatMessages.getCurrentPage().iterator(); iterator.hasNext();) {
			ChatMessage chatMessage = (ChatMessage) iterator.next();
			List<Value> values = new ArrayList<Value>();
			values.add(new StringValue(chatMessage.from.user.id));
			values.add(new StringValue(chatMessage.id));
			values.add(new StringValue(chatMessage.body.content));
			ZonedDateTime createdDate = chatMessage.createdDateTime.toInstant().atZone(zoneId);
			values.add(new DateTimeValue(createdDate));
			values.add(new StringValue(chatMessage.messageType.name()));
			values.add(new StringValue((chatMessage.replyToId) == null ? "" : chatMessage.replyToId));
			rows.add(new Row(values));
		}	

		while (chatMessages.getNextPage() != null) {
			chatMessages = chatMessages.getNextPage().buildRequest().get();
				for (Iterator iterator = chatMessages.getCurrentPage().iterator(); iterator.hasNext();) {
					ChatMessage chatMessage = (ChatMessage) iterator.next();
					List<Value> values = new ArrayList<Value>();
					values.add(new StringValue(chatMessage.from.user.id));
					values.add(new StringValue(chatMessage.id));
					values.add(new StringValue(chatMessage.body.content));
					values.add(new DateTimeValue(chatMessage.createdDateTime.toInstant().atZone(zoneId)));
					values.add(new StringValue(chatMessage.messageType.name()));
					values.add(new StringValue((chatMessage.replyToId) == null ? "" : chatMessage.replyToId));
					rows.add(new Row(values));
				}
		}

		Table table = new Table();
		table.setRows(rows);
		table.setSchema(schemas);

		TableValue tableValue= new TableValue();
		tableValue.set(table);

		return tableValue;
	
	}

}
