/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.microsoft.graph.models.extensions.Chat;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.IChatCollectionPage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(return_label = "Chat IDs", node_label = "Get User Chats", 
label = "Get User Chats", description = "Get User Teams Chat IDs", 
name = "teamsuserchats", icon = "pkg.svg", return_type = DataType.LIST, comment = true ,background_color = "#302c89"  ,return_required = true)
public class GetTeamsUserChats{
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
	public ListValue<String> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,default_value = "MSTeamsSession") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "User ID", default_value_type = STRING) @NotEmpty String userID) {

    	List<Value> chatIDs = new ArrayList<Value>();
		
   	 	IGraphServiceClient graphClient  = (IGraphServiceClient) this.sessions.get(sessionName); 
   	 	
		IChatCollectionPage chats = graphClient.users(userID).chats()
			    .buildRequest()
			    .get();
		
		if (chats != null) {
			for (Iterator iterator = chats.getCurrentPage().iterator(); iterator.hasNext();) {
					Chat chat = (Chat) iterator.next();
				chatIDs.add(new StringValue(chat.id));

			}

			while (chats.getNextPage() != null) {
				chats = chats.getNextPage().buildRequest().get();
				for (Iterator iterator = chats.getCurrentPage().iterator(); iterator.hasNext();) {
					Chat chat = (Chat) iterator.next();
					chatIDs.add(new StringValue(chat.id));

				}
			}
		}
	
		
		ListValue returnValue = new ListValue();
		returnValue.set(chatIDs);
		return returnValue;
			
	}
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
	
}
