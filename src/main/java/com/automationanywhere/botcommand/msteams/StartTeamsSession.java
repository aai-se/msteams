/*
 * Copyright (c) 2020 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

/**
 * 
 */
package com.automationanywhere.botcommand.msteams;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.core.security.SecureString;
import com.microsoft.graph.models.extensions.IGraphServiceClient;

/**
 * Sessions provide a mechanism to communicate between actions of the same
 * package. This means that sessions belonging to other packages are not visible
 * to action.
 * <p>
 * In this example we will check if a session( we will use string objects,
 * though in real actions a more complicated Object might be used) exists and if
 * not we will add it.
 * 
 * @author Raj Singh Sisodia
 *
 */
@BotCommand
@CommandPkg(label = "Start Session", name = "startTeamsSession", description = "Start New Session {{sessionName}}" ,icon = "pkg.svg",  node_label = "Start Session {{sessionName}}" ,comment = true ,background_color =  "#302c89" )
public class StartTeamsSession{

	// Sessions are provided as a Map. Actions can add or remove entries in this
	// Map.
	// The choice to reuse/overwrite/delete/add any Object in this Map belongs to
	// the actions, and the framework makes no assumption regarding it.
	@Sessions
	private Map<String, Object> sessions;
	
	private static final Logger logger = LogManager.getLogger(StartTeamsSession.class);

	@Execute
	public void execute( @Idx(index = "1", type = TEXT) @Pkg(label = "Session Name",  default_value_type = STRING, default_value = "MSTeamsSession") @NotEmpty String sessionName,
						 @Idx(index = "2", type = AttributeType.CREDENTIAL) @Pkg(label = "User Name",  default_value_type = STRING) @NotEmpty SecureString username,
						 @Idx(index = "3", type = AttributeType.CREDENTIAL) @Pkg(label = "Password",  default_value_type = STRING) @NotEmpty SecureString password,
						 @Idx(index = "4", type = AttributeType.CREDENTIAL) @Pkg(label = "Tenant ID",  default_value_type = STRING) @NotEmpty SecureString tenantid,
						 @Idx(index = "5", type = AttributeType.CREDENTIAL) @Pkg(label = "Client ID",  default_value_type = STRING) @NotEmpty SecureString clientid,
						 @Idx(index = "6", type = AttributeType.CREDENTIAL) @Pkg(label = "Client Secret Key",  default_value_type = STRING) @NotEmpty SecureString secretkey) 
	
	{
		   if (sessions.containsKey(sessionName))
	            throw new BotCommandException("Session name in use") ;
	       
		   try {
			   
			   IGraphServiceClient client = MSAuthHelper.newClientbyUserCredentials(clientid.getInsecureString(), username.getInsecureString(), password.getInsecureString(),tenantid.getInsecureString(), secretkey.getInsecureString());
		       this.sessions.put(sessionName, client);
		        
		    } catch (Exception exception) {
		        logger.error("Authorization failed.", (Throwable)exception);
		        throw new BotCommandException("InvalidUserCredentials : "+exception.getMessage());
		    }
		
	}

	// Ensure that a public setter exists.
	public void setSessions(Map<String, Object> sessionMap) {
		this.sessions = sessionMap;
	}

}
