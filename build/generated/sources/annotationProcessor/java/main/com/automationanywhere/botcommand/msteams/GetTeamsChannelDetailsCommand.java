package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetTeamsChannelDetailsCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetTeamsChannelDetailsCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetTeamsChannelDetails command = new GetTeamsChannelDetails();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("teamID") && parameters.get("teamID") != null && parameters.get("teamID").get() != null) {
      convertedParameters.put("teamID", parameters.get("teamID").get());
      if(convertedParameters.get("teamID") !=null && !(convertedParameters.get("teamID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","teamID", "String", parameters.get("teamID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("teamID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","teamID"));
    }

    if(parameters.containsKey("channelID") && parameters.get("channelID") != null && parameters.get("channelID").get() != null) {
      convertedParameters.put("channelID", parameters.get("channelID").get());
      if(convertedParameters.get("channelID") !=null && !(convertedParameters.get("channelID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","channelID", "String", parameters.get("channelID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("channelID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","channelID"));
    }

    if(parameters.containsKey("getMessages") && parameters.get("getMessages") != null && parameters.get("getMessages").get() != null) {
      convertedParameters.put("getMessages", parameters.get("getMessages").get());
      if(convertedParameters.get("getMessages") !=null && !(convertedParameters.get("getMessages") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","getMessages", "Boolean", parameters.get("getMessages").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("getMessages") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","getMessages"));
    }

    if(parameters.containsKey("last") && parameters.get("last") != null && parameters.get("last").get() != null) {
      convertedParameters.put("last", parameters.get("last").get());
      if(convertedParameters.get("last") !=null && !(convertedParameters.get("last") instanceof ZonedDateTime)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","last", "ZonedDateTime", parameters.get("last").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("teamID"),(String)convertedParameters.get("channelID"),(Boolean)convertedParameters.get("getMessages"),(ZonedDateTime)convertedParameters.get("last")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
