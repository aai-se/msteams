package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class SendTeamsChannelMessageCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(SendTeamsChannelMessageCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    SendTeamsChannelMessage command = new SendTeamsChannelMessage();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("teamID") && parameters.get("teamID") != null && parameters.get("teamID").get() != null) {
      convertedParameters.put("teamID", parameters.get("teamID").get());
      if(convertedParameters.get("teamID") !=null && !(convertedParameters.get("teamID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","teamID", "String", parameters.get("teamID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("teamID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","teamID"));
    }

    if(parameters.containsKey("channelID") && parameters.get("channelID") != null && parameters.get("channelID").get() != null) {
      convertedParameters.put("channelID", parameters.get("channelID").get());
      if(convertedParameters.get("channelID") !=null && !(convertedParameters.get("channelID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","channelID", "String", parameters.get("channelID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("channelID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","channelID"));
    }

    if(parameters.containsKey("message") && parameters.get("message") != null && parameters.get("message").get() != null) {
      convertedParameters.put("message", parameters.get("message").get());
      if(convertedParameters.get("message") !=null && !(convertedParameters.get("message") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","message", "String", parameters.get("message").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("message") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","message"));
    }

    if(parameters.containsKey("format") && parameters.get("format") != null && parameters.get("format").get() != null) {
      convertedParameters.put("format", parameters.get("format").get());
      if(convertedParameters.get("format") !=null && !(convertedParameters.get("format") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","format", "String", parameters.get("format").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("format") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","format"));
    }
    if(convertedParameters.get("format") != null) {
      switch((String)convertedParameters.get("format")) {
        case "HTML" : {

        } break;
        case "TEXT" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","format"));
      }
    }

    if(parameters.containsKey("messageID") && parameters.get("messageID") != null && parameters.get("messageID").get() != null) {
      convertedParameters.put("messageID", parameters.get("messageID").get());
      if(convertedParameters.get("messageID") !=null && !(convertedParameters.get("messageID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","messageID", "String", parameters.get("messageID").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("teamID"),(String)convertedParameters.get("channelID"),(String)convertedParameters.get("message"),(String)convertedParameters.get("format"),(String)convertedParameters.get("messageID")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
