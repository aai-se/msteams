package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.bot.service.TriggerException;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import com.automationanywhere.toolchain.runtime.Trigger3;
import com.automationanywhere.toolchain.runtime.Trigger3ListenerContext;
import java.lang.ClassCastException;
import java.lang.Double;
import java.lang.NullPointerException;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ChannelTriggerTrigger implements Trigger3 {
  private static final Logger logger = LogManager.getLogger(ChannelTriggerTrigger.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  private static ChannelTriggerTrigger thisInstance = new ChannelTriggerTrigger();

  private final ChannelTrigger command = new ChannelTrigger();

  private ChannelTriggerTrigger() {
    super();
  }

  public static ChannelTriggerTrigger getInstance() {
    return thisInstance;
  }

  public Object clone() {
    return thisInstance;
  }

  public void startListen(Trigger3ListenerContext triggerListenerContext,
      Map<String, Value> parameters) throws TriggerException {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null);
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("username") && parameters.get("username") != null && parameters.get("username").get() != null) {
      convertedParameters.put("username", parameters.get("username").get());
      if(convertedParameters.get("username") !=null && !(convertedParameters.get("username") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","username", "SecureString", parameters.get("username").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("username") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","username"));
    }

    if(parameters.containsKey("password") && parameters.get("password") != null && parameters.get("password").get() != null) {
      convertedParameters.put("password", parameters.get("password").get());
      if(convertedParameters.get("password") !=null && !(convertedParameters.get("password") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","password", "SecureString", parameters.get("password").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("password") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","password"));
    }

    if(parameters.containsKey("tenantid") && parameters.get("tenantid") != null && parameters.get("tenantid").get() != null) {
      convertedParameters.put("tenantid", parameters.get("tenantid").get());
      if(convertedParameters.get("tenantid") !=null && !(convertedParameters.get("tenantid") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","tenantid", "SecureString", parameters.get("tenantid").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("tenantid") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","tenantid"));
    }

    if(parameters.containsKey("clientid") && parameters.get("clientid") != null && parameters.get("clientid").get() != null) {
      convertedParameters.put("clientid", parameters.get("clientid").get());
      if(convertedParameters.get("clientid") !=null && !(convertedParameters.get("clientid") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","clientid", "SecureString", parameters.get("clientid").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("clientid") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","clientid"));
    }

    if(parameters.containsKey("secretkey") && parameters.get("secretkey") != null && parameters.get("secretkey").get() != null) {
      convertedParameters.put("secretkey", parameters.get("secretkey").get());
      if(convertedParameters.get("secretkey") !=null && !(convertedParameters.get("secretkey") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","secretkey", "SecureString", parameters.get("secretkey").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("secretkey") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","secretkey"));
    }

    if(parameters.containsKey("teamID") && parameters.get("teamID") != null && parameters.get("teamID").get() != null) {
      convertedParameters.put("teamID", parameters.get("teamID").get());
      if(convertedParameters.get("teamID") !=null && !(convertedParameters.get("teamID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","teamID", "String", parameters.get("teamID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("teamID") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","teamID"));
    }

    if(parameters.containsKey("channelID") && parameters.get("channelID") != null && parameters.get("channelID").get() != null) {
      convertedParameters.put("channelID", parameters.get("channelID").get());
      if(convertedParameters.get("channelID") !=null && !(convertedParameters.get("channelID") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","channelID", "String", parameters.get("channelID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("channelID") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","channelID"));
    }

    if(parameters.containsKey("interval") && parameters.get("interval") != null && parameters.get("interval").get() != null) {
      convertedParameters.put("interval", parameters.get("interval").get());
      if(convertedParameters.get("interval") !=null && !(convertedParameters.get("interval") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","interval", "Double", parameters.get("interval").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("interval") == null) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","interval"));
    }
    if(convertedParameters.containsKey("interval")) {
      try {
        if(convertedParameters.get("interval") != null && !((double)convertedParameters.get("interval") > 0)) {
          throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.GreaterThan","interval", "0"));
        }
      }
      catch(ClassCastException e) {
        throw new TriggerException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","interval", "Number", convertedParameters.get("interval").getClass().getSimpleName()));
      }
      catch(NullPointerException e) {
        throw new TriggerException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","interval"));
      }
      if(convertedParameters.get("interval")!=null && !(convertedParameters.get("interval") instanceof Number)) {
        throw new TriggerException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","interval", "Number", convertedParameters.get("interval").getClass().getSimpleName()));
      }

    }
    command.setTriggerUid(triggerListenerContext.getTriggerUid());
    try {
      command.startTrigger((SecureString)convertedParameters.get("username"),(SecureString)convertedParameters.get("password"),(SecureString)convertedParameters.get("tenantid"),(SecureString)convertedParameters.get("clientid"),(SecureString)convertedParameters.get("secretkey"),(String)convertedParameters.get("teamID"),(String)convertedParameters.get("channelID"),(Double)convertedParameters.get("interval"));}
    catch (ClassCastException e) {
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.IllegalParameters","startTrigger"));
    }
    catch (TriggerException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public void stopListen(String triggerUid) throws TriggerException {
    try {
      command.stopListen(triggerUid);}
    catch (TriggerException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public void stopAllTriggers() throws TriggerException {
    try {
      command.stopAllTriggers();}
    catch (TriggerException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new TriggerException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
