package com.automationanywhere.botcommand.msteams;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class StartTeamsSessionCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(StartTeamsSessionCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    StartTeamsSession command = new StartTeamsSession();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("username") && parameters.get("username") != null && parameters.get("username").get() != null) {
      convertedParameters.put("username", parameters.get("username").get());
      if(convertedParameters.get("username") !=null && !(convertedParameters.get("username") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","username", "SecureString", parameters.get("username").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("username") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","username"));
    }

    if(parameters.containsKey("password") && parameters.get("password") != null && parameters.get("password").get() != null) {
      convertedParameters.put("password", parameters.get("password").get());
      if(convertedParameters.get("password") !=null && !(convertedParameters.get("password") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","password", "SecureString", parameters.get("password").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("password") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","password"));
    }

    if(parameters.containsKey("tenantid") && parameters.get("tenantid") != null && parameters.get("tenantid").get() != null) {
      convertedParameters.put("tenantid", parameters.get("tenantid").get());
      if(convertedParameters.get("tenantid") !=null && !(convertedParameters.get("tenantid") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","tenantid", "SecureString", parameters.get("tenantid").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("tenantid") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","tenantid"));
    }

    if(parameters.containsKey("clientid") && parameters.get("clientid") != null && parameters.get("clientid").get() != null) {
      convertedParameters.put("clientid", parameters.get("clientid").get());
      if(convertedParameters.get("clientid") !=null && !(convertedParameters.get("clientid") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","clientid", "SecureString", parameters.get("clientid").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("clientid") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","clientid"));
    }

    if(parameters.containsKey("secretkey") && parameters.get("secretkey") != null && parameters.get("secretkey").get() != null) {
      convertedParameters.put("secretkey", parameters.get("secretkey").get());
      if(convertedParameters.get("secretkey") !=null && !(convertedParameters.get("secretkey") instanceof SecureString)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","secretkey", "SecureString", parameters.get("secretkey").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("secretkey") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","secretkey"));
    }

    command.setSessions(sessionMap);
    try {
      command.execute((String)convertedParameters.get("sessionName"),(SecureString)convertedParameters.get("username"),(SecureString)convertedParameters.get("password"),(SecureString)convertedParameters.get("tenantid"),(SecureString)convertedParameters.get("clientid"),(SecureString)convertedParameters.get("secretkey"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","execute"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
